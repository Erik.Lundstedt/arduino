void setAllPixels(int r,int g,int b)
{
	for (int i=0; i<=pixNum; i++)
	{
		writeEasyNeoPixel(i, r, g, b);
	  delay(delayval);
	}
}

void incPixels()
{
	for (int i=0; i<=pixNum; i++)
	{
		writeEasyNeoPixel(i, 0, 0, i*4);
		delay(delayval);
	}
}


void rainbowPixels(int r,int g,int b,int inc,int i)
{
	for (i; i<=pixNum; i=i+inc)
	{
		writeEasyNeoPixel(i, r, g, b);
		delay(delayval);
	}
}
void SetPixels(int startPix,int endPix,int r,int g,int b)
{
	for (int i=startPix; i<=endPix; i++)
	{
		writeEasyNeoPixel(i, r, g, b);
		delay(delayval);
	}
}

void chase(int r,int g,int b)
{
    for (int i=0;i<=pixNum; i+=3) 
	{
		writeEasyNeoPixel(i+0,r,g,b);
		writeEasyNeoPixel(i+1,r,g,b);
		writeEasyNeoPixel(i+2,r,g,b);
		delay(delayval);
		writeEasyNeoPixel(i-0,0,0,0);
    writeEasyNeoPixel(i-1,0,0,0);
    writeEasyNeoPixel(i-2,0,0,0);
	}
	
	delayval=0;
	setAllPixels(0,0,0);
  delayval=50;
  
}

void chase2(int r,int g,int b)
{
    for (int i=0;i<=pixNum; i+=3) 
	{
	  
		writeEasyNeoPixel(i,rgb[0],rgb[1],rgb[2]);
		writeEasyNeoPixel(i+1,rgb[0],rgb[1],rgb[2]);
		writeEasyNeoPixel(i+2,rgb[0],rgb[1],rgb[2]);

		delay(delayval);
		writeEasyNeoPixel(i, 0,0,0);
	  writeEasyNeoPixel(i-1, 0,0,0);
	  writeEasyNeoPixel(i-2, 0,0,0);

    writeEasyNeoPixel(i-3,rgb[0],rgb[1],rgb[2]);
    writeEasyNeoPixel(i-4,rgb[0],rgb[1],rgb[2]);
    writeEasyNeoPixel(i-5,rgb[0],rgb[1],rgb[2]);
    
		delay(delayval);
		writeEasyNeoPixel(i-3, 0,0,0);
	  writeEasyNeoPixel(i-4, 0,0,0);
	  writeEasyNeoPixel(i-5, 0,0,0);
	}
	
	delayval=0;
	setAllPixels(0,0,0);
  delayval=50;
  
}








//    writeEasyNeoPixel(i, 0, 0, 100);

void rep()
{
	while (Serial.available() > 0)
	{
		// look for the next valid integer in the incoming serial stream:
		int red = Serial.parseInt();
		// do it again:
		int green = Serial.parseInt();
		// do it again:
		int blue = Serial.parseInt();

		// look for the newline. That's the end of your sentence:
		if (Serial.read() == '\n')
		{
			// constrain the values to 0 - 255 and invert
			// if you're using a common-cathode LED, just use "constrain(color, 0, 255);"

			red =   constrain(red, 0, 255);

			green = constrain(green, 0, 255);

			blue =  constrain(blue, 0, 255);

			rgb[0]=red;
			rgb[1]=green;
			rgb[2]=blue;

			// print the three numbers in one string as hexadecimal:
			Serial.print(rgb[0]);
			Serial.print(",");
			Serial.print(rgb[1]);
			Serial.print(",");
			Serial.println(rgb[2]);
      }
    }
}