package it.lundstedt.erik;

import it.lundstedt.erik.menu.Menu;
import jssc.SerialPort;
import jssc.SerialPortException;

public class Main
{

public static void main(String[] args) {

	try {
	String[] ports=jssc.SerialPortList.getPortNames();
	String[] portsHeadder={"select a port","starts at 0"};
		String port;
	//    for (int i = 0; i < ports.length; i++) {
		//    System.out.println(port);
		
		
		port = UsrConf.defaultPort;
		if (UsrConf.doAskForPort)
		{
			Menu portMenu=new Menu(portsHeadder,ports);
			portMenu.drawItems();
			port=ports[Menu.getChoice()];
		}
		SerialPort serial=new SerialPort(port);
		serial.openPort();
		Thread.sleep(Settings.delay);
		System.out.println("connected to port "+serial.getPortName());
		//Menu.getInput("data to send")
		
		boolean doExit=false;
		
		
		while (!doExit) {
			Menu colourMenu = new Menu(UsrConf.ColourHead, UsrConf.colourNames);
			colourMenu.drawMenu();
			int colour = Menu.getChoice();
			if (colour==0)
			{
				doExit=true;
			}
			else {
				serial.writeString(UsrConf.rgbColours[colour - 1] + "\n");
				
			}
			Thread.sleep(10);
		}
		//serial.closePort();
		UsrConf.exit(serial);

	} catch (SerialPortException | InterruptedException e) {
		e.printStackTrace();
	}
	
}





}
