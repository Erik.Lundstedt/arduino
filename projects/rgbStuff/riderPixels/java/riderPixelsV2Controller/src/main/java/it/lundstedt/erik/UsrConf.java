package it.lundstedt.erik;
import jssc.SerialPort;
import jssc.SerialPortException;
public class UsrConf
{
//portSettings
 public static boolean doAskForPort=true;
 public static String defaultPort="/dev/ttyACM0";

 public static String red=RGB.red;
 public static String green=RGB.green;
 public static String blue=RGB.blue;
 public static String pink=RGB.pink;
 public static String yellow=RGB.yellow;
 public static String orange=RGB.orange;
 public static String format="red,green,blue";
 public static String off="0,0,0";
 public static String[] rgbColours ={          red ,  green ,  blue ,  off ,  pink ,  yellow ,  orange};
 public static String[] colourNames={"0exit","1red","2green","3blue","4off","5pink","6yellow","7orange"};

 static int length=colourNames.length-1;
 public static String[] ColourHead ={"pick one","accepts integers between 1 and "+length};


 
 
 
 
 
public static void exit(SerialPort serial) throws InterruptedException, SerialPortException {
 String[] message={"exiting ",".",".",".\n","closing serial port ",".",".",".\n","done!"};//8
 for (int i = 0; i < message.length ; i++) {
  System.out.print(message[i]);
  Thread.sleep(500);
  if (i==4)//if message is closing serial
  {
   Thread.sleep(500);
   if (serial.isOpened()) serial.closePort();/*close the serial port*/
  }
 }
 System.exit(1);
}


 
 
}
