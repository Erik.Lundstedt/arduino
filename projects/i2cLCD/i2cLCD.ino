#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x3F, 16, 2);
String inputBuffer="";
void setup() {
  lcd.init(); // begins connection to the LCD module
  lcd.backlight(); // turns on the backlight
  lcd.clear(); 

  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect.
  }

  lcd.setCursor(0, 0); // set cursor to first row
  lcd.print("Init done"); // print to lcd
 
}

void loop() {
      lcd.setCursor(0, 0); // set cursor to first row
      lcd.print("I received: "); // print out to LCD
  if (Serial.available() > 0) {
      inputBuffer=Serial.readStringUntil('\n');
      delay(70);
  }
      lcd.setCursor(0, 1); // set cursor to second row
      lcd.print(inputBuffer); // print out the retrieved value to the second row

}
