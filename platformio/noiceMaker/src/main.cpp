#include <Arduino.h>
#include "U8glib.h"
#include <EasyNeoPixels.h>



U8GLIB_SH1106_128X64 u8g(U8G_I2C_OPT_NONE);	// I2C / TWI
//U8GLIB_SH1106_128X64 u8g(U8G_I2C_OPT_DEV_0|U8G_I2C_OPT_FAST);	// Dev 0, Fast I2C / TWI
//U8GLIB_SH1106_128X64 u8g(U8G_I2C_OPT_NO_ACK);	// Display which does not send ACK


int potVar=0;


void draw(int potvar) {

  //potVar=map(analogRead(A3),0,256,0,256);
  // graphic commands to redraw the complete screen should be placed here
  u8g.setFont(u8g_font_unifont);
  //u8g.setFont(u8g_font_osb21);
  u8g.drawFrame(0+5-1, 9, 102, 12);
  u8g.drawBox( 1+5, 11, map(potVar,0,1023,0,98), 8);
  u8g.drawBox(100+5,12,5,6);
  u8g.setPrintPos(1+5,45);
  u8g.print(map(potVar,0,1023,0,100));
  u8g.print("%");
}

void setup(void) {
  // flip screen, if required
  // u8g.setRot180();

  // set SPI backup if required
  //u8g.setHardwareBackup(u8g_backup_avr_spi);

  // assign default color value
  if ( u8g.getMode() == U8G_MODE_R3G3B2 ) {
    u8g.setColorIndex(255);     // white
  }
  else if ( u8g.getMode() == U8G_MODE_GRAY2BIT ) {
    u8g.setColorIndex(3);         // max intensity
  }
  else if ( u8g.getMode() == U8G_MODE_BW ) {
    u8g.setColorIndex(1);         // pixel on
  }
  else if ( u8g.getMode() == U8G_MODE_HICOLOR ) {
    u8g.setHiColorByRGB(255,255,255);
  }

  pinMode(8, OUTPUT);
  pinMode(A3,INPUT);
  setupEasyNeoPixels(9, 8);
}


void pixelmap0(int num, int per)
{
  for (int pixel = num; pixel <= 8; pixel++) {
    writeEasyNeoPixel(pixel, 0, 0, 0);
  }
  for (int pixel = 0; pixel <= num; pixel++) {
    if (per==0) {
      writeEasyNeoPixel(pixel, 0, 0, 0);
    } else {
      writeEasyNeoPixel(pixel, 0, 0, 100);
    }
  }

}







void mapPixels(int number)
{
  int ledArr[8][3];
  ledArr[0][1]=100;
  ledArr[1][1]=100;
  ledArr[2][1]=100;
  ledArr[3][1]=100;
  ledArr[4][1]=100;
  ledArr[5][1]=100;
  ledArr[6][1]=100;
  ledArr[7][1]=100;
  ledArr[8][1]=100;


  int num=map(number,0,1023,0,8);
  int per=map(number,0,1023,0,100);
  int binar=map(number, 0, 1203,0,16);

  switch (binar) {
    case 0:

    break;
    case 1:

    break;
    case 2:

    break;
    case 3:

    break;
    case 4:

    break;
    case 5:

    break;
    case 6:

    break;
    case 7:

    break;
    case 8:

    break;
    case 9:

    break;
    case 10:

    break;
    case 11:

    break;
    case 12:

    break;
    case 13:

    break;
    case 14:

    break;
    case 15:

    break;
    case 16:

    break;



  }

}





void loop(void) {





  potVar=analogRead(A3);

  // picture loop
  u8g.firstPage();
  do {
    draw(potVar);
  } while( u8g.nextPage() );


  mapPixels(potVar);

  // rebuild the picture after some delay
  delay(50);
}
