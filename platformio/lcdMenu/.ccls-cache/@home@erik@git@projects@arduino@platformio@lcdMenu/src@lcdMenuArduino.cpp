/////////////////////////////////////////////////////////////////
#include <Adafruit_NeoPixel.h>
#include <EasyNeoPixels.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <RotaryEncoder.h>
#include <Button2.h>



/////////////////////////////////////////////////////////////////

#define BUTTON_PIN 5

/////////////////////////////////////////////////////////////////

Button2 button = Button2(BUTTON_PIN);
LiquidCrystal_I2C lcd(0x27, 20, 4);
RotaryEncoder encoder(A2, A3);

/////////////////////////////////////////////////////////////////
int buttonPin= BUTTON_PIN;

String text[]={"hello world","1","2","3"};


void updateDisplay()
{
  lcd.clear();
  for (int i = 0; i <4; i++)
  {
    lcd.setCursor(0,i);
    lcd.print(text[i]);
  }
}

void flashDisplay(int index,int time, String text)
{
  lcd.setCursor(0, index);
  lcd.print("                    ");
  lcd.setCursor(0, index);
  lcd.print(text);
  delay(time);
  updateDisplay();
  
}
  int pos = 0;
  int newPos;

int encoderPos()
{
encoder.tick();
newPos=constrain(encoder.getPosition(), 0, 10);
if (pos != newPos) {
    pos = newPos;
    text[0]=pos;
    text[1]=encoder.getPosition();
//    text[3]=digitalRead(buttonPin);
    updateDisplay();
    encoder.setPosition(pos);
  }
  return pos;
}

void resetPos()
{
    pos=0;
    newPos=0;
    encoder.setPosition(0);
    text[0]=pos;
    text[1]=encoder.getPosition();
    updateDisplay();
}

void pressed(Button2& btn)
{
  resetPos();
  flashDisplay(2, 900, "Reset!");
}


void setup()
{

  // button.setChangedHandler(changed);
   button.setPressedHandler(pressed);
  //button.seReleasedHandler(released);

  // button.setTapHandler(tap);
  //button.setClickHandler(clicked);
  //button.setLongClickHandler(longClick);
  //button.setDoubleClickHandler(doubleClick);
  //button.setTripleClickHandler(tripleClick);
  



    
  lcd.init();
  lcd.backlight();
  lcd.setCursor(0,0);
  //lcd.print("Hello, world!");
  updateDisplay();
}

void rep()
{
  int ePos=encoderPos();
    
}

void loop()
{
  button.loop();
  rep();
  if (5 <= 10) {
  
}
  
}

// Local Variables:
// eval: (treemacs)
// eval: (pretty-symbols)
// treemacs-width: 50
// End:
