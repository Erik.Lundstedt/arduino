/////////////////////////////////////////////////////////////////
#include <Adafruit_NeoPixel.h>
#include <EasyNeoPixels.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <RotaryEncoder.h>
#include <Button2.h>



/////////////////////////////////////////////////////////////////

#define BUTTON_PIN 5
#define MENUITEMS 10
/////////////////////////////////////////////////////////////////

Button2 button = Button2(BUTTON_PIN);
LiquidCrystal_I2C lcd(0x27, 20, 4);
RotaryEncoder encoder(A2, A3);





/////////////////////////////////////////////////////////////////

int delayTime=50;
int firstRing=24;
int secondRing=16;
int thirdRing=12;
int numPix=firstRing+secondRing+thirdRing;





int rgb[]={0,0,10};



int buttonPin= BUTTON_PIN;
int pos = 0;
int newPos;


String text[]={"0","1","2","3"};
//                                                     \ / \ / \ /
String menuItems[]={"warn","RED","GREEN","BLUE","confirm","4","5","6","7","8","9"," "," ","!"};



void updatePixels()
{
    for (int i=0; i < numPix; i++)
    {
        writeEasyNeoPixel(i, rgb[0],rgb[1],rgb[2]);
        delay(100);
    }   
}
void updateDisplay()
{
  lcd.clear();
  for (int i = 0; i <4; i++)
  {
    lcd.setCursor(0,i);
    lcd.print(text[i]);
  }
  //updatePixels();
}

void flashDisplay(int index,int time, String text)
{
  lcd.setCursor(0, index);
  lcd.print("                    ");
  lcd.setCursor(0, index);
  lcd.print(text);
  delay(time);
  updateDisplay();
  
}

void setDisplayArr(String newText[])
{
  for (int i = 0; i < 4; i++)
  { 
    text[i]=newText[i];
  }
}

void resetPos(int setPos)
{
    pos=setPos;
    newPos=setPos;
    encoder.setPosition(setPos);
    text[0]=menuItems[pos];
    text[1]=menuItems[pos+1];
    text[2]=menuItems[pos+2];
    text[3]=menuItems[pos+3];
    updateDisplay();
    delay(100);
}

int encoderPos()
{
encoder.tick();
newPos=encoder.getPosition();
    if (pos != newPos) {
      pos = newPos;
    text[0]=menuItems[pos];
    text[1]=">  "+menuItems[pos+1]+"  <";
    text[2]=menuItems[pos+2];
    //text[3]=menuItems[pos+3]+"  "+rgb[0]+","+rgb[1]+","+rgb[2];

    text[3]=(String)rgb[0]+" "+rgb[1]+" "+rgb[2];
if (newPos > 10) {
    resetPos(10);
}

if (newPos < 0) {
  resetPos(0);
}
updateDisplay();
encoder.setPosition(pos);
  }
  return pos;
}


int encoderPosInt(int oldValue,int min,int max)
{
    encoder.setPosition(oldValue);
    encoder.tick();
    int value=oldValue;
    int newValue=encoder.getPosition();
    if (value != newValue)
    {
        value = newValue;
        if (newPos > max)
        {
        resetPos(max);
        }
        if (newPos < min)
        {
            resetPos(min);
        }
        updateDisplay();
        encoder.setPosition(value);
    }
return value;
}

void select ()
{
 switch (pos) {
   case 0:
     text[3]="red";
     rgb[0]=encoderPosInt(rgb[0],0,255);
     break;
   case 1:
     text[3]="green";
     break;
   case 2:
     text[3]="blue";
     break;
   case 3:
     text[3]="three";
     break;
   case 4:
     String items[]={"this","is","menuitem","four"};
     setDisplayArr(items);
     
     break;
 }
updateDisplay(); 
}

void pressed(Button2& btn)
{
  select();
}

void longClick(Button2& btn)
{


 unsigned int time = btn.wasPressedFor();
    Serial.print("You clicked ");
    if (time > 1500) {
        Serial.print("a really really long time.");
        resetPos(0);
        flashDisplay(2, 900, "Reset!");
    }
    else if (time > 1000)
    {
      Serial.print("a really long time.");
      resetPos(0);
      flashDisplay(2, 900, "Reset!");
    }
    else if(time > 500)
    {
      Serial.print("a long time.");        
    }
    else
    {
      Serial.print("long.");        
    }
    Serial.print(" (");        
    Serial.print(time);        
    Serial.println(" ms)");



}


void setup()
{

  // button.setChangedHandler(changed);
   button.setPressedHandler(pressed);
  //button.seReleasedHandler(released);

  // button.setTapHandler(tap);
  //button.setClickHandler(clicked);
  button.setLongClickHandler(longClick);
  //button.setDoubleClickHandler(doubleClick);
  //button.setTripleClickHandler(tripleClick);
  


    setupEasyNeoPixels(8, numPix);

    
  lcd.init();
  lcd.backlight();
  lcd.setCursor(0,0);
  //lcd.print("Hello, world!");
//  encoderPos();
  updateDisplay();
}

void rep()
{
  int ePos=encoderPos();
    
}

void loop()
{
  button.loop();
  rep();



  
}

// Local Variables:
// eval: (treemacs)
// eval: (pretty-symbols)
// treemacs-width: 50
// End:
