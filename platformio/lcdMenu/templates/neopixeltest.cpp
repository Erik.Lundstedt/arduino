
#include <EasyNeoPixels.h>
int delayTime=50;
int firstRing=24;
int secondRing=16;
int thirdRing=12;


int numPix=firstRing+secondRing+thirdRing;
void setup() {
  // put your setup code here, to run once:
    setupEasyNeoPixels(8, numPix);
}

void loop() {
  // put your main code here, to run repeatedly:
    for (int i=0; i < firstRing; i++)
    {
        writeEasyNeoPixel(i, 60,0,0);
        delay(delayTime);
    }

    for (int i=firstRing; i < firstRing+secondRing; i++)
    {
        writeEasyNeoPixel(i, 0,60,0);
        delay(delayTime);
    }


    for (int i=firstRing+secondRing; i < numPix; i++)
    {
        writeEasyNeoPixel(i, 0,0,60);
        delay(delayTime);
    }



    // turn the NeoPixels OFF
    for (int i = 0; i < numPix; i++)
    {
        writeEasyNeoPixel(i, LOW);
        delay(delayTime/2);
    }

}
