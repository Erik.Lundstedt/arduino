int liquidSensor=A0;
int ledPin[3]={2,3,4};//red,green,blue
int RGBVal[3]={0,0,0};
int resistance;
void toggle(int pin)
{
  digitalWrite(pin,!digitalRead(pin));
}

void red() {
  toggle(ledPin[0]);
}
void yellow() {
  toggle(ledPin[1]);
}
void green() {
  toggle(ledPin[2]);
}

void RGB() {
  digitalWrite(ledPin[0],RGBVal[0]);
  digitalWrite(ledPin[1],RGBVal[1]);
  digitalWrite(ledPin[2],RGBVal[2]);
}
void setRGB(int r,int g,int b)
  {
    RGBVal[0]=r;
    RGBVal[1]=g;
    RGBVal[2]=b;
  }



  void rep(int in)
  {
    Serial.println(in);
    if (in>=40) {
      Serial.println("mer");
      setRGB(1,0,0);
    }
    else if (in<=41 and in>=19)
    {
      Serial.println("lagom");
      setRGB(0,1,0);
    }
    else if (in<=20)
    {
      Serial.println("mindre");
      setRGB(0,0,1);
    }
    else
    {
      Serial.println(".......");
    }

  }

  void setup() {
    // put your setup code here, to run once:
    pinMode(liquidSensor,INPUT);
    pinMode(ledPin[0],OUTPUT);
    pinMode(ledPin[1],OUTPUT);
    pinMode(ledPin[2],OUTPUT);
    Serial.begin(9600);
  }

  void loop() {
    // put your main code here, to run repeatedly:

    resistance=map(analogRead(liquidSensor),0,1023,0,64);

    rep(resistance);

    RGB();



  }
