#include <Arduino.h>
#include <Wire.h>
#include <MicroLCD.h>
long timer=0;
LCD_SH1106 lcd; /* for SH1106 OLED module */
//LCD_SSD1306 lcd; /* for SSD1306 OLED module */

void setup()
{
	lcd.begin();
}


void loop()
{
	lcd.clear();
	lcd.setCursor(0, 0);
	lcd.setFontSize(FONT_SIZE_XLARGE);
	lcd.printLong(timer);
	delay(1000);
	timer++;
}
