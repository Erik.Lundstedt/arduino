#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x3f,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display

#include <EasyNeoPixels.h>
int VRX=A0;
int VRY=A1;
int SW=4;
int Data[2];
int i;
int curCol;
int pot=A2;


const int pixnum=60;
const int pixpin=5;
const int button=6;
int Info[2]={0,0};
int col[4]={0,0,0,0};
void pixel(int pixNum,int r,int g,int b)
{
	writeEasyNeoPixel(pixNum,r,g,b);
}
void setup() {
	// put your setup code here, to run once:
	pinMode (VRX,INPUT);
	pinMode (VRY,INPUT);
	pinMode (SW,INPUT);

	pinMode (pot,INPUT);


	setupEasyNeoPixels(pixpin,pixnum);
	Serial.begin(9600);
	lcd.init();
	lcd.backlight();
}
void loop()
{
	// put your main code here, to run repeatedly:
	int xvr=map(analogRead(VRX),0,1023,0,256);//maps the yoystick to 0 to 256 midle is 125
	int yvr=map(analogRead(VRY),0,1023,0,256);//maps the yoystick to 0 to 256 midle is 125
	Data[0]=xvr;
	Data[1]=yvr;

	int potVal=map(analogRead(pot),0,1023,1,4);
	lcd.setCursor(0,1);
	lcd.print(potVal);
	lcd.print("   ");
	col[potVal]=xAxis();
/*
	col[1];
	col[2];
	col[3]=xAxis();
*/

	Serial.print("X: ");
	Serial.print(Data[0]);

	lcd.setCursor(0,0);
	lcd.print("X:");
	lcd.print(Data[0]);

	//lcd.print("");

	Serial.print(" ,Y: ");
	Serial.println(Data[1]);
	lcd.setCursor(5,0);
	lcd.print("Y:");
	lcd.print(Data[1]);

	lcd.print(".");
/*
		lcd.print();

*/
	lcd.setCursor(10,0);
	Serial.println(Info[0]);
	lcd.print(".");
	lcd.print(Info[0]);
	lcd.print(".");

	Serial.println(Info[1]);

	//lcd.setCursor(14,0);
	lcd.print(Info[1]);
	lcd.print("   ");


//int xvr=map(analogRead(VRX),0,1023,0,256);
//maps the yoystick to 0 to 256 midle is 125





	lcd.setCursor(0,1);
	col[0]=yAxis();

	pixel(col[0],col[1],col[2],col[3]);
	delay(100);
}
