/*
Colors on a single Neopixel!
*/

#include <EasyNeoPixels.h>
int del = 250;
int pixels = 10;
int maxBright=70;
int pixel[4];

void setPixel()
{
	writeEasyNeoPixel(pixel[0],pixel[1],pixel[2],pixel[3]);
}

void setup() {
	setupEasyNeoPixels(13, pixels);
}

void loop()
{
	for(int x=1;x<3;x++)
	{
		pixel[x]=10;
		for (int i = 0; i < pixels;i++)
		{
			pixel[0]=i;
			delay(del);
			setPixel();
		}
	}

}
