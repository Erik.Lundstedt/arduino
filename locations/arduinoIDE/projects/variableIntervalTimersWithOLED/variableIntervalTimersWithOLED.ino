#include <Arduino.h>
#include <Wire.h>
#include <MicroLCD.h>
int timers[2];
int ledPin=8;
int potPins[]={A0,A1,A2,A3,A4,A5};
int A=1;
int row[5];

LCD_SH1106 lcd; /* for SH1106 OLED module */
//LCD_SSD1306 lcd; /* for SSD1306 OLED module */

void printDelay() {
  lcd.clear();
	timers[0]=row[0];
  timers[1]=row[1];
  timers[2]=row[2];
  timers[3]=row[3];
  timers[4]=row[4];
  //timers[5]=row[5];

  lcd.setCursor(0, 0);
  lcd.printLong(timers[0]*timers[3]);
  lcd.setCursor(0, 2);
  lcd.printLong(timers[1]*timers[3]);
  lcd.setCursor(0, 4);
  lcd.printLong(timers[2]*timers[3]);
  lcd.setCursor(30, 0);
  lcd.printLong(timers[3]);
//  lcd.setCursor(0, 4);
//  lcd.printLong(timers[4]);
  /*lcd.setCursor(0, 5);
  lcd.printLong(timers[5]);
*/
  delay(100);
}
void toggle(int pin)
{
  digitalWrite(pin,!digitalRead(pin));
  delay(100);
  digitalWrite(pin,!digitalRead(pin));
}






void setup()
{
  lcd.begin();

  pinMode(potPins[0],INPUT);
  pinMode(potPins[1],INPUT);
  pinMode(potPins[2],INPUT);
  pinMode(potPins[3],INPUT);
  pinMode(potPins[4],INPUT);



  pinMode(ledPin,OUTPUT);
lcd.setCursor(0, 0);
lcd.setFontSize(FONT_SIZE_MEDIUM);
Serial.begin(9600);
}


void loop()
{
  row[0]=map(analogRead(potPins[0]),0,1023,0,10);
  row[1]=map(analogRead(potPins[1]),0,1023,0,10);
  row[2]=map(analogRead(potPins[2]),0,1023,0,10);
  row[3]=map(analogRead(potPins[3]),0,1023,1,10);
  //row[4]=map(analogRead(potPins[4]),0,1023,0,10);
  //row[5]=map(analogRead(potPins[5]),0,1023,0,10);
printDelay();
Serial.println(timers[0]);
Serial.println(timers[1]);

toggle(ledPin);
delay(timers[0]*timers[3]);
toggle(ledPin);
delay(timers[1]*timers[3]);
toggle(ledPin);
delay(timers[2]*timers[3]);
toggle(ledPin);
//delay(timers[3]);


}
