#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

#include <LiquidCrystal_I2C.h>


LiquidCrystal_I2C lcd(0x3F, 16, 2);



/*15*/
/**/
const char* ssid = "your ssid";
const char* password = "pasword";

ESP8266WebServer server(80);

const int led=13;
void clear()
{
	lcd.setCursor(7,1);
	lcd.print("        ");
}



void toggle(int pin)
{
	digitalWrite(pin,!digitalRead(pin));
	server.send(200, "text/plain", "toggled led pin /*13*/");
}
void handleToggle()
{
	clear();
	toggle(13);
	lcd.setCursor(0,1);

	lcd.print("pin 13:");
	lcd.print(digitalRead(led));
}



void handleRoot() {
	toggle(13);
	server.send(200, "text/plain", "hello from esp8266!");
	toggle(13);
}
void handleNotFound()
{
	toggle(13);
	String message = "File Not Found\n\n";
	message += "URI: ";
	message += server.uri();
	message += "\nMethod: ";
	message += (server.method() == HTTP_GET)?"GET":"POST";
	message += "\nArguments: ";
	message += server.args();
	message += "\n";
	for (uint8_t i=0; i<server.args(); i++){
		message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
	}
	server.send(404, "text/plain", message);
	toggle(13);
}

void setup(void)
{
	pinMode(led, OUTPUT);
	Serial.begin(115200);
	WiFi.mode(WIFI_STA);
	WiFi.begin(ssid, password);
	Serial.println("");
	lcd.begin(16,2);
	lcd.init();
	// Wait for connection
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}
	Serial.println("");
	Serial.print("Connected to ");
	Serial.println(ssid);
	Serial.print("IP address: ");
	lcd.setCursor(0, 0);
	lcd.print("IP ");
	lcd.setCursor(3, 0);
	lcd.print(WiFi.localIP());
	Serial.println(WiFi.localIP());

	if (MDNS.begin("esp8266"))
	{
		Serial.println("MDNS responder started");
	}
	server.on("/", handleRoot);
	server.on("/tgl",handleToggle);
lcd.backlight();

	server.onNotFound(handleNotFound);

	server.begin();
	Serial.println("HTTP server started");
}

void loop(void){
	server.handleClient();
}
