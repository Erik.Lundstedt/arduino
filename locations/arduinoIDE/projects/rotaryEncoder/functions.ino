void disp(/* arguments */)
{
  lcd.clear();
  lcd.setFontSize(FONT_SIZE_SMALL);
  lcd.println("Hello, world!");
  lcd.setFontSize(FONT_SIZE_MEDIUM);
  lcd.println("Hello, world!");
  delay(1000);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.setFontSize(FONT_SIZE_SMALL);
  lcd.printLong(12345678);
  delay(1000);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.setFontSize(FONT_SIZE_MEDIUM);
  lcd.printLong(12345678);
  delay(1000);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.setFontSize(FONT_SIZE_LARGE);
  lcd.printLong(12345678);
  delay(1000);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.setFontSize(FONT_SIZE_XLARGE);
  lcd.printLong(12345678);
  delay(1000);
}




void timerIsr() {
  encoder->service();
}










void lcdPrintTwoLines(String line1,String line2)
{
  lcd.println(line1);
  lcd.println(line2);
}

void lcdPrintSmall(String line1,String line2)
{
  lcd.clear();
  lcd.setFontSize(FONT_SIZE_SMALL);
  lcdPrintTwoLines(line1,line2);
}


void displayAccelerationStatus() {
  lcd.setCursor(0, 0);
  lcdPrintSmall("Acceleration ",encoder->getAccelerationEnabled() ? "on " : "off");
}
