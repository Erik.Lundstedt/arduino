#include <EasyNeoPixels.h>
#include <Adafruit_NeoPixel.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
/*
const char* ssid = "your ssid";
const char* password = "pasword";
*/

const char* ssid = "dlink";
const char* password = "05480548";



int col[4]={0,0,0,0};

int i;

ESP8266WebServer server(80);

const int numPixels = 16;
const int led=13;



void pixel(int pixNum,int r,int g,int b)
{
	writeEasyNeoPixel(pixNum,r,g,b);
}

void turnOff()
{
	pixel(0,0,0,0);
	pixel(1,0,0,0);
	pixel(2,0,0,0);
	pixel(3,0,0,0);
	pixel(4,0,0,0);
	pixel(5,0,0,0);
	pixel(6,0,0,0);
	pixel(7,0,0,0);
	pixel(8,0,0,0);
	pixel(9,0,0,0);
	pixel(10,0,0,0);
	pixel(11,0,0,0);
	pixel(12,0,0,0);
	pixel(13,0,0,0);
	pixel(14,0,0,0);
	pixel(15,0,0,0);
	col[0]=0;
}
void turnOn(int r,int g,int b)
{
  for (i)
	{
		pixel(0,r,g,b);
  }

  col[0]=0;
}

void toggle(int pin)
{
	digitalWrite(pin,!digitalRead(pin));
	server.send(200, "text/plain", "toggled led pin /*13*/");
}
void handleToggle()
{
	toggle(13);
}

void handleOff()
{
	turnOff();
}
void handleAdd(){if(col[0]<=0){col[0]=col[0]+1;}}
void handleSubtract(){if(col[0]>=15){col[0]=col[0]-1;}}
void handleZero(){col[0]=0;}

/*void handleTest(int var)
{
Serial.println(var);
}
*/
void handleRedOn()
{
	turnOn(150,0,0);
	server.send(200, "text/plain", "turned red on");
}
void handleRedOff()
{
	col[1]=0;
	server.send(200, "text/plain", "turned red off");
}
void handleGreenOn()
{
	col[2]=150;
	server.send(200, "text/plain", "turned green on");
}
void handleGreenOff()
{
	col[2]=0;
	server.send(200, "text/plain", "turned green off");
}
void handleBlueOn()
{
	col[3]=150;
	server.send(200, "text/plain", "turned blue on");
}
void handleBlueOff()
{
	col[3]=0;
	server.send(200, "text/plain", "turned blue off");
}

void handleRoot() {
	toggle(13);
	String msg = "hello from esp8266!\n\n";
	msg += "blue: ";
	msg += col[0] ;
	server.send(200, "text/plain", msg);
	toggle(13);
}
void handleNotFound()
{
	toggle(13);
	String message = "File Not Found\n\n";
	message += "URI: ";
	message += server.uri();
	message += "\nMethod: ";
	message += (server.method() == HTTP_GET)?"GET":"POST";
	message += "\nArguments: ";
	message += server.args();
	message += "\n";
	for (uint8_t i=0; i<server.args(); i++){
		message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
	}
	server.send(404, "text/plain", message);
	toggle(13);
}

void setup(void)
{
	pinMode(led, OUTPUT);
	setupEasyNeoPixels(5, numPixels);//pin D7
	Serial.begin(115200);
	WiFi.mode(WIFI_STA);
	WiFi.begin(ssid, password);
	Serial.println("*");
	Serial.println("*");
	Serial.println("*");
	turnOn(150,0,0);
	// Wait for connection
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}
	Serial.println("");
	Serial.print("Connected to ");
	Serial.println(ssid);
	Serial.print("IP address: ");
	Serial.println(WiFi.localIP());

	if (MDNS.begin("esp8266"))
	{
		Serial.println("MDNS responder started");
	}
	server.on("/", handleRoot);
	server.on("/tgl",handleToggle);
	server.on("/red/on",handleRedOn);
	server.on("/red/off",handleRedOff);
	server.on("/green/on",handleGreenOn);
	server.on("/green/off",handleGreenOff);
	server.on("/blue/on",handleBlueOn);
	server.on("/blue/off",handleBlueOff);

	server.on("/pixel/add",handleAdd);
	server.on("/pixel/subtract",handleSubtract);
	server.on("/zero",handleZero);

	server.on("/off",handleOff);
	//server.on("/test",handleTest(5));



	turnOff();
	server.onNotFound(handleNotFound);

	server.begin();
	Serial.println("HTTP server started");
}

void loop(void){
	server.handleClient();



	//	for (i=0;i<=numPixels;i++);{}



	pixel(col[0],col[1],col[2],col[3]);


	/*
	Serial.print(" pixel ");
	Serial.println(col[0]);
	*/
	Serial.print("colour: ");
	Serial.print(" red ");
	Serial.print(col[1]);
	Serial.print(" green ");
	Serial.print(col[2]);
	Serial.print(" blue ");
	Serial.println(col[3]);
}
