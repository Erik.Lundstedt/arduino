#include <EasyNeoPixels.h>

#include <Adafruit_NeoPixel.h>
//#include <EasyNeopixels.h>
int pixpin=6;
int numpix=30;

int pixel[4]={0,0,0,0};

Adafruit_NeoPixel strip = Adafruit_NeoPixel(numpix, pixpin, NEO_GRB + NEO_KHZ800);

// Fill the dots one after the other with a color
void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, c);
    strip.show();
    delay(wait);
  }
}




void clear()
{

  colorWipe(strip.Color(0, 0, 0), 1); //clear


}

void setup()
{
  setupEasyNeoPixels(pixpin, numpix);
  Serial.begin(9600);
  /*
  if (Serial.available() > 0)
  {
  serial.print("ready");
}
*/

//clear();

}

void rep()
{
  while (Serial.available() > 0)
  {

    int pixNum = Serial.parseInt();
    // look for the next valid integer in the incoming serial stream:
    int red = Serial.parseInt();
    // do it again:
    int green = Serial.parseInt();
    // do it again:
    int blue = Serial.parseInt();

    // look for the newline. That's the end of your sentence:
    if (Serial.read() == '\n')
    {
      // constrain the values to 0 - 255 and invert
      // if you're using a common-cathode LED, just use "constrain(color, 0, 255);"
      pixNum =  constrain(pixNum, 0, numpix);
      red =  constrain(red, 0, 255);
      green =  constrain(green, 0, 255);
      blue =  constrain(blue, 0, 255);
      pixel[0]=pixNum;
      pixel[1]=red;
      pixel[2]=green;
      pixel[3]=blue;
      // print the three numbers in one string as hexadecimal:
      Serial.print(pixNum, HEX);
      Serial.print(red, HEX);
      Serial.print(green, HEX);
      Serial.println(blue, HEX);
    }
  }




}









void  colorSequence()
{
  // make it red
  writeEasyNeoPixel(1, 255, 0, 0);
  delay(500);
  // make it green
  writeEasyNeoPixel(1, 0, 255, 0);
  delay(500);
  // make it blue
  writeEasyNeoPixel(1, 0, 0, 255);
  delay(500);
}



void loop()
{
  rep();
  writeEasyNeoPixel(pixel[0],pixel[1],pixel[2],pixel[3]);
}

