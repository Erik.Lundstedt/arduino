/*
IR Receiver Demonstration 3
IR-Rcv-Demo3.ino
Control LED's using Unused IR Remote keys

DroneBot Workshop 2017
http://dronebotworkshop.com
*/

// Include IR Remote Library by Ken Shirriff
#include <IRremote.h>

#include <Adafruit_NeoPixel.h>
#include <EasyNeoPixels.h>
#include <Wire.h>
#include <Adafruit_TiCoServo.h>

#define SERVO_PIN    9
#define SERVO_MIN 1000 // 1 ms pulse
#define SERVO_MAX 2000 // 2 ms pulse


Adafruit_TiCoServo servo;



// Define sensor pin
const int RECV_PIN = 4;

/* LED pins
const int redPin = 13;
const int yellowPin = 12;
*/


int pixpin=3;
//int pixnum=8;
int pixnum=60;

int red=0;
int green=0;
int blue=0;

int color[4]={pixnum,red,green,blue};
int currentColor=99;
int incval=10;
int decval=10;

int x=90;
int pos = 0;
int result;

// Define integer to remember toggle state
int togglestate = 0;

int lastservo;


// Define IR Receiver and Results Objects
IRrecv irrecv(RECV_PIN);
decode_results results;
void setup()
{
  setupEasyNeoPixels(pixpin, pixnum);
  Serial.begin(9600);
  // Enable the IR Receiver
  irrecv.enableIRIn();
  pixoff();
  /*LedPins
   Set LED pins as Outputs
  pinMode(redPin, OUTPUT);
  pinMode(yellowPin, OUTPUT);
  */
  servo.attach(SERVO_PIN, SERVO_MIN, SERVO_MAX);
}


void loop()
{
  if (irrecv.decode(&results))
  {
    rep();
    colourTranslate();
    pixel();
    debug(false);
    //delay(5);
    irrecv.resume();
  }
  servo.write(x);
}
