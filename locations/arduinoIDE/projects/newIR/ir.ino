/***********base functions***********/
void setRed()
{
  currentColor=1;
}
void setGreen()
{
  currentColor=2;
}
void setBlue()
{
  currentColor=3;
}
void reset()
{
  red=0;
  green=0;
  blue=0;
  for (int i = 1; i <=3; i++)
  {
    color[i]=0;
  }
}
void unknowncode()
{

  Serial.println("UNKNOWN hexCode:");
  Serial.println("to add it to your code");
  Serial.println("");
  Serial.println("");
  Serial.println("");
  Serial.print("case ");
  Serial.print("0x");
  Serial.print(results.value, HEX);
  Serial.println(":");
  Serial.println("/*your code here*/");
  Serial.println("");
  Serial.println("");
  Serial.println("");
  Serial.println("break;");
  Serial.println("");
  Serial.println("");
  Serial.println("");
}

/***********sub functions***********/
/***********uses base function******/
void colourTranslate()
{
  red=color[1];
  green=color[2];
  blue=color[3];
}
/**********simple functions*********/
/**********uses sub functions*******/
void incPix(){
  if (color[0]<=pixnum)
  {
    color[0]++;
  }
}
void decPix(){
  if (color[0]>=0)
  {
    color[0]--;
  }
}
void incCol(){
  switch (currentColor)
  {
    case 1:
    if (color[1]<=250)
    {
      color[1]=color[1]+incval;
    }
    break;
    case 2:
    if (color[2]<=250)
    {
      color[2]=color[2]+incval;
    }
    break;
    case 3:
    if (color[3]<=250)
    {
      color[3]=color[3]+incval;
    }
    break;
    case 99://starting value

    break;
  }
  colourTranslate();
}
void decCol(){
  switch (currentColor)
  {
    case 1:
    if (color[1]>=0)
    {
      color[1]=color[1]-decval;
    }
    break;
    case 2:
    if (color[2]>=0)
    {
      color[2]=color[2]-decval;
    }
    break;
    case 3:
    if (color[3]>=0)
    {
      color[3]=color[3]-decval;
    }
    break;
    case 99://starting value

    break;
  }
  colourTranslate();
}
/***************main loop***********/
void rep()
{
  if (irrecv.decode(&results))
  {
    result=results.value;
    switch(result)
    {
      case 0x538://red
      setRed();
      break;

      case 0x1D0://green
      setGreen();
      break;

      case 0x870://blue
      setBlue();
      break;

      case 0x90://chanel ++
      incPix();
      break;
      case 0x890://chanel --
      decPix();
      break;


      case 0x490://volume +
      incCol();
      break;

      case 0xC90://volume -
      decCol();
      break;

      case 0xA90://off button
      reset();
      break;
      case 0x290://off button
      reset();
      break;

      case 0x6183EEBB:
      switch (togglestate)
      {
        case 0://
        for (int i = 1; i < 3; i++)
        {
          color[i]=255;
        }
        togglestate=1;
        break;
        case 1:
        for (int i = 1; i < 3; i++)
        {
          color[i]=0;
        }
        togglestate=0;
        break;
      }
      break;
      case 0x5D0:
      switch (togglestate)
      {
        case 0://
        for (int i = 1; i < 3; i++)
        {
          color[i]=255;
        }
        togglestate=1;
        break;
        case 1:
        for (int i = 1; i < 3; i++)
        {
          color[i]=0;
        }
        togglestate=0;
        break;
      }
      break;

      case 0x2F0://INC
      servoLeft();
      break;
      case 0xAF0://DEC
      servoRight();
      break;

      case 0xA70://return to "0"
      servoReset();
      break;
      /*
      case 0x655205B7:
      servoReset();
      break;
      case 0x414F8E71:
      servoReset();
      break;
      case ://

      break;
      case ://

      break;
      case ://

      break;
      case ://

      break;
      case ://

      break;
      */
      /*preset buttons*/
      /*

      case 0x1:
      pixrainbow();
      break;
      case 0x801:
      pixrainbow();
      break;

      case 0x2:
      color[0]=16;
      break;
      case 0x802:
      color[0]=16;
      break;

      */

      default:
      if (irrecv.decode(&results))
      {
        /*
        Serial.println("UNKNOWN hexCode:");
        Serial.println(results.value, HEX);
        */
        unknowncode();
      }
    }
  }
}


void debug(bool enabeled)
{
  if (enabeled==true)
  {
    Serial.println("hexCode:");
    Serial.print("0x");
    Serial.println(results.value, HEX);
    Serial.println("colours: ");
    Serial.print("red: ");
    Serial.println(color[1]);
    Serial.println(red);
    Serial.print("green: ");
    Serial.println(color[2]);
    Serial.println(green);
    Serial.print("blue: ");
    Serial.println(color[3]);
    Serial.println(blue);
    Serial.print("currentColor");
    Serial.println(currentColor);
  }
  else if(enabeled==false)
  {
    /*
    Serial.println("colours: ");
    Serial.print("red: ");
    Serial.println(color[1]);
    Serial.println(red);
    Serial.print("green: ");
    Serial.println(color[2]);
    Serial.println(green);
    Serial.print("blue: ");
    Serial.println(color[3]);
    Serial.println(blue);
    Serial.print("pixel nr: ");
    Serial.println(color[0]);
    */

  }

}
