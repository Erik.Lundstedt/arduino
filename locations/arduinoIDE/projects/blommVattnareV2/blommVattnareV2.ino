
#include <Adafruit_NeoPixel.h>//includes the NeoPixel library by adafruit
#define PIN 2	 // input pin Neopixel is attached to
#define NUMPIXELS 1 // number of neopixels to use
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);//initializes the neopixel library by adafruit
int liquidSensor=A0;//the pin nr of the sensor
int RGBVal[3]={0,0,0};//red,green,blue
int resistance;
void RGB()
{
  for(int i=0;i<NUMPIXELS;i++)
  {
    // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
    pixels.setPixelColor(i, pixels.Color(RGBVal[0], RGBVal[1], RGBVal[2])); // Moderately bright green color.

    }
    pixels.show(); // This sends the updated pixel color to the hardware.
  }

void setRGB(int r,int g,int b)//sets the RGBVal to the red,green,blue values passed
{
  RGBVal[0]=r;//sets RGBVal to the inputed values
  RGBVal[1]=g;//sets RGBVal to the inputed values
  RGBVal[2]=b;//sets RGBVal to the inputed values
}



void rep(int in)//child to the "loop()" function. will repeat as the loop runs,main if else/switch case: statements here
}
{
  Serial.println(in);
  if (in>=40) {
    Serial.println("too much water!!!!!!!");//prints info about amount of water to the serial port on the arduino
    setRGB(0,0,100);//sets the RGB values acordingly
  }
  else if (in<=41 and in>=19)
  {
    Serial.println("just enough water");//prints info about amount of water to the serial port on the arduino
    setRGB(0,100,0);//sets the RGB values acordingly
  }
  else if (in<=20)
  {
    Serial.println("give me more water please!");//prints info about amount of water to the serial port on the arduino
    setRGB(100,0,0);//sets the RGB values acordingly
  }
  else
  {
    Serial.println(".......");//does pretty mutch nothing
  }

}

void setup() {
  // put your setup code here, to run once:
  pinMode(liquidSensor,INPUT);
  pixels.begin(); // Initializes the NeoPixel library.
  Serial.begin(9600);// Initializes the Serial port
}
void loop()
{
  // put your main code here, to run repeatedly:
  resistance=map(analogRead(liquidSensor),0,1023,0,64);//maps the 0 to 1023 sensor value to the less exact but more handleable 0 to 64
  rep(resistance);//calls the rep() function with the variable /*int*/resistance as a parameter
  RGB();



}
