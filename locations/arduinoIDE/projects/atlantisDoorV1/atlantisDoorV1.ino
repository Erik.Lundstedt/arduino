#include <EasyNeoPixels.h>

const int pixnum=8;
const int pixpin=6;



int inputPin=2; //ECHO pin
int outputPin=4; //TRIG pin

void setup()
{

	Serial.begin(9600);
	pinMode(inputPin, INPUT);
	pinMode(outputPin, OUTPUT);
	setupEasyNeoPixels(pixpin,pixnum);
	pixel(5,0,0,10);
	delay(500);
	pixel(5,0,0,0);
}

void loop()
{
	rep();
	delay(10);
}
