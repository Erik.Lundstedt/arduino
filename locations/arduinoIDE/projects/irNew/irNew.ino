#include "IRRemoteControl.h"

IRRecv irRecv;


const int IR_RECV_PIN = 2;
                               /*650*/
const int MAX_IR_BUFFER_LENGTH = 9;
unsigned int irBuffer[MAX_IR_BUFFER_LENGTH];
int currentIrBufferLength = 0;

const int FREQ_KHZ = 40;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);
  irRecv.start(IR_RECV_PIN, irBuffer, MAX_IR_BUFFER_LENGTH, currentIrBufferLength);
}

void loop() {
  // put your main code here, to run repeatedly:

Serial.println(F("Press the remote control button now - only once"));
  delay(5000);
  
  if (currentIrBufferLength > 0) {
    irRecv.stop(IR_RECV_PIN);
    
    digitalWrite(LED_BUILTIN, HIGH);
    
    Serial.println();
    Serial.println(F("Code: "));
    
    for (int i = 0; i < currentIrBufferLength; i++) {
      if (i > 0) {
        Serial.print(F(", "));
      }
      Serial.print(irBuffer[i]);
    }
    digitalWrite(LED_BUILTIN, LOW);
  }
  irRecv.start(IR_RECV_PIN, irBuffer, MAX_IR_BUFFER_LENGTH, currentIrBufferLength);
}
