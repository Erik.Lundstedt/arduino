#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

/*const char* ssid = "dlink";
const char* password = "05480548";
*/
const char* ssid = "NTIElev";
const char* password = "7pQA2gy59HbK";

ESP8266WebServer server(80);

const int left = 5;
const int right=16;
const int led=13;
void toggle(int pin)
{
	digitalWrite(pin,!digitalRead(pin));
	server.send(200, "text/plain", "toggled led pin /*13*/");
}
void handleToggle()
{
	toggle(13);
}
void handleLeft()
{
	toggle(left);
}
void handleRight()
{
	toggle(right);
}
void handleForward()
{
	toggle(left);
	toggle(right);
}


void handleRoot() {
	digitalWrite(led, 1);
	server.send(200, "text/plain", "hello from esp8266!");
	digitalWrite(led, 0);
}
void handleNotFound()
{
	digitalWrite(led, 1);
	String message = "File Not Found\n\n";
	message += "URI: ";
	message += server.uri();
	message += "\nMethod: ";
	message += (server.method() == HTTP_GET)?"GET":"POST";
	message += "\nArguments: ";
	message += server.args();
	message += "\n";
	for (uint8_t i=0; i<server.args(); i++){
		message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
	}
	server.send(404, "text/plain", message);
	digitalWrite(led, 0);
}

void setup(void)
{
	pinMode(left, OUTPUT);
	pinMode(right,OUTPUT);
	digitalWrite(left, 0);
	digitalWrite(right,0);
	Serial.begin(115200);
	WiFi.mode(WIFI_STA);
	WiFi.begin(ssid, password);
	Serial.println("");

	// Wait for connection
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}
	Serial.println("");
	Serial.print("Connected to ");
	Serial.println(ssid);
	Serial.print("IP address: ");
	Serial.println(WiFi.localIP());

	if (MDNS.begin("esp8266"))
	{
		Serial.println("MDNS responder started");
	}
	server.on("/", handleRoot);
	server.on("/toggle",handleToggle);
	server.on("/left",handleLeft);
	server.on("/right",handleRight);
	server.on("/forward",handleForward);

	server.onNotFound(handleNotFound);

	server.begin();
	Serial.println("HTTP server started");
}

void loop(void){
	server.handleClient();
}
