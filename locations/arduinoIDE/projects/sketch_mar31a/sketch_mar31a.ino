int indicate=3;
int VRX=A0;
int VRY=A1;
int SW=2;
/*
X: 1023 ,Y: 1023

int map(int x, int 0, int 1023, int 1, int 10)
*/
void setup() {
  // put your setup code here, to run once:
   pinMode (indicate,OUTPUT);
   pinMode (VRX,INPUT);
   pinMode (VRY,INPUT);
	 pinMode (SW,INPUT);
   Serial.begin(9600);
}

void loop()
{
  // put your main code here, to run repeatedly:
  int sw=digitalRead(SW);
	int xvr=map(analogRead(VRX),0,1023,1,64);
	int yvr=map(analogRead(VRY),0,1023,1,64);
	int Data[2]={xvr,yvr};
	Serial.print("X: ");
	Serial.print(Data[0]);
	Serial.print(" ,Y: ");
  Serial.println(Data[1]);

	delay(100);
}
