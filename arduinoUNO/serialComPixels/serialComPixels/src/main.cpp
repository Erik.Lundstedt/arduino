#include <Arduino.h>

#include <Adafruit_NeoPixel.h>

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define PIN            6

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      60

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

int pixel[4]={0,0,0,0};


int delayval = 500; // delay for half a second

void writeNeoPixel(int i,int r, int g, int b)
{
	// pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
	pixels.setPixelColor(i, pixels.Color(r,g,b)); // Moderately bright blue color.
	pixels.show(); // This sends the updated pixel color to the hardware.
	// delay(delayval); // Delay for a period of time (in milliseconds).
}
void rep()
{
	while (Serial.available() > 0)
	{

		int pixNum = Serial.parseInt();
		// look for the next valid integer in the incoming serial stream:
		int red = Serial.parseInt();
		// do it again:
		int green = Serial.parseInt();
		// do it again:
		int blue = Serial.parseInt();

		// look for the newline. That's the end of your sentence:
		if (Serial.read() == '\n')
		{
			// constrain the values to 0 - 255 and invert
			// if you're using a common-cathode LED, just use "constrain(color, 0, 255);"
			pixNum =  constrain(pixNum, 0, NUMPIXELS);

			red =  constrain(red, 0, 255);

			green =  constrain(green, 0, 255);

			blue =  constrain(blue, 0, 255);

			pixel[0]=pixNum;
			pixel[1]=red;
			pixel[2]=green;
			pixel[3]=blue;
			// print the three numbers in one string as hexadecimal:
			Serial.print(red /*,HEX*/);
			Serial.print(green /*,HEX*/);
			Serial.println(blue/*, HEX*/);
		}
	}

}






void setup()
{
	Serial.begin(9600);
	pixels.begin(); // This initializes the NeoPixel library.
}

void loop()
{

	rep();
	int pixnum,red,green,blue;
	pixnum=pixel[0];
	red=pixel[1];
	green=pixel[2];
	blue=pixel[3];
	writeNeoPixel(pixnum,red,green,blue);
	//pixels.setPixelColor(pixel[0],pixel[1],pixel[2],pixel[3]); // Moderately bright blue color.
	pixels.show();
}
