#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
int pixpin=6;
int numpix=12;
//28
int pixel[4]={0,0,0,0};
Adafruit_NeoPixel easyNeoPixels = Adafruit_NeoPixel(numpix, pixpin, NEO_GRB + NEO_KHZ800);

// set the nth neopixel to a particular rgb color
void writeEasyNeoPixel(int num, int r, int g, int b) {
  easyNeoPixels.setPixelColor(num, easyNeoPixels.Color(r,g,b));
  easyNeoPixels.show();
}


void clear()
{
	 for (int i = 0; i < numpix; i++)
	 {
	 	writeEasyNeoPixel(i,0,0,0);
	 }
}



void rep()
{
	while (Serial.available() > 0)
	{

		int pixNum = Serial.parseInt();
		// look for the next valid integer in the incoming serial stream:
		int red = Serial.parseInt();
		// do it again:
		int green = Serial.parseInt();
		// do it again:
		int blue = Serial.parseInt();

		// look for the newline. That's the end of your sentence:
		if (Serial.read() == '\n')
		{
			// constrain the values to 0 - 255 and invert
			// if you're using a common-cathode LED, just use "constrain(color, 0, 255);"
			pixNum =  constrain(pixNum, 0, numpix);

			red =  constrain(red, 0, 255);

			green =  constrain(green, 0, 255);

			blue =  constrain(blue, 0, 255);

			pixel[0]=pixNum;
			pixel[1]=red;
			pixel[2]=green;
			pixel[3]=blue;
			// print the three numbers in one string as hexadecimal:
			Serial.print(pixNum, HEX);
			Serial.print(red, HEX);
			Serial.print(green, HEX);
			Serial.println(blue, HEX);
		}
	}

}

void  colorSequence()
{
	// make it red
	writeEasyNeoPixel(1, 255, 0, 0);
	delay(500);
	// make it green
	writeEasyNeoPixel(1, 0, 255, 0);
	delay(500);
	// make it blue
	writeEasyNeoPixel(1, 0, 0, 255);
	delay(500);
}

void setup()
{
	easyNeoPixels.begin();
	Serial.begin(9600);
	clear();
	/*
	if (Serial.available() > 0)
	{
	serial.print("ready");
}
*/
}


void loop()
{
	//clear();
	rep();
	writeEasyNeoPixel(pixel[0],pixel[1],pixel[2],pixel[3]);
}
