#include <Arduino.h>

#include <Adafruit_NeoPixel.h>
int pixpin=7;
int numpix=60;

//int pixel[4]={0,0,0,0};
Adafruit_NeoPixel strip = Adafruit_NeoPixel(numpix, pixpin, NEO_GRB + NEO_KHZ800);





void colorWipe(uint32_t c, uint8_t wait) {
	for(uint16_t i=0; i<strip.numPixels(); i++) {
		strip.setPixelColor(i, c);
		strip.show();
		delay(wait);
	}
}
void clear()
{

	colorWipe(strip.Color(0, 0, 0), 0); //clear


}
void setup()
{
	Serial.begin(9600);
	strip.begin();
	strip.show(); // Initialize all pixels to 'off'
}

void loop()
{
colorWipe(strip.Color(10, 0, 0), 10);
delay(100);
colorWipe(strip.Color(0, 10, 0), 10);
delay(100);
colorWipe(strip.Color(0, 0, 10), 10);
delay(100);
}
