// HID - Version: Latest
#include <Arduino.h>
#include <HID.h>
#include <Keyboard.h>
#include <EasyNeoPixels.h>
/*
rubber duckino
requires pro-micro or leonardo
*/
int pixNum=1;
const int maxVal=128;//115200
int pixPin=15;
int buttonState = 0;
void setup()
{
  pinMode(4, INPUT_PULLUP);
  pinMode(13, OUTPUT);
  setupEasyNeoPixels(pixPin, pixNum);
  Serial.begin(9600);
  Serial.print("hello world");
}
void blink(int R,int G,int B)
{
  digitalWrite(13, HIGH);
  writeEasyNeoPixel(0,R,G,B);
  delay(100); // Delay a little bit to improve simulation performance
  digitalWrite(13, LOW);
  writeEasyNeoPixel(0,0,0,0);
  delay(100); // Delay a little bit to improve simulation performance
}
void typeKey(uint8_t key)
{
  Keyboard.press(key);
  delay(50);
  Keyboard.release(key);
}
void payload()
{
  Keyboard.print(F("CTRL-ALT R"));

  Keyboard.print(F("echo hello world"));

  typeKey(KEY_RETURN);
}
void run()
{
  writeEasyNeoPixel(0,100,0,0);
  payload();
  delay(1000);
  Serial.println("done");
  writeEasyNeoPixel(0,0,0,0);
}

void loop()
{
  writeEasyNeoPixel(0,100,100,0);
  // read the state of the pushbutton value
  buttonState = digitalRead(4);
  // check if pushbutton is pressed. if it is, the
  // buttonState is HIGH
  if (buttonState == HIGH) {
    blink(100,0,0);
    Keyboard.begin();
    run();
    // Keyboard.end();
    delay(100); // Delay a little bit to improve simulation performance
    Keyboard.end();
    blink(0,0,100);
  } else {
    // turn LED off
    digitalWrite(13, LOW);
    writeEasyNeoPixel(0,0,0,0);
  }
  //delay(10); // Delay a little bit to improve simulation performance
}
